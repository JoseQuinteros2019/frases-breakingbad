import React from 'react';

const Phrases = (props) => {
    return (
        <div className="frase">
            <h1> {props.frase} </h1>
            <p> {props.autor} </p>
        </div>
    );
};

export default Phrases;