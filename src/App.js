import React,{useState,useEffect} from 'react';
import logo from './logo.svg';
import './index.css';
import Phrases from './Components/Phrases';

function App() {
  // useState es un hook 
  // creamos un state con hook 
  // const [nombrestate,funcion modificar similar setState]
  const [frase, setFrase] = useState([]);
  //setFrase= this.setState
  //useEffect nos deja utilizar lo que seria el ciclo de vida de un componente es la union de didmount y didupdate
  useEffect(() => {
    //aqui va codigo
    consultaApi();

  },
  
    []// a quien mira react para cambiar
  );
  // asi se crea una funcion const nombre de la funcion
  const consultaApi = async () => {
    const url = 'http://breaking-bad-quotes.herokuapp.com/v1/quotes';
    const consulta = await fetch(url);
    const respuesta = await consulta.json();
    console.log(respuesta[0]);
    // guardar en el state frase
    setFrase(respuesta[0]);
    
  }
  
  return (
    <div className="contenedor" >
      <Phrases frase={frase.quote} autor={frase.author} />
      <button onClick={()=> consultaApi ()} > Generar frase</button>
         </div>
  );
}

export default App;
